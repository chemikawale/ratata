Centaurus Bootstrap 3 Theme

CSS Styles (in css folder):
- The /compiled folder includes all major styles of the theme.
- The /bootstrap folder includes the bootstrap files. Bootsrap libs are not changed.
- The /lib folder includes the styles for the plugins.
- Added classes .hidden-xxs and .visible-xss for showing/hiding content on 420px's devices and smaller.

Scripts:
- Javascript code for every page is located at the end of each html template file, so you don't have to include all plugins for every page.

Icons:
- FontAwesome icons
- Glyphicons - if you move the location of bootstrap files, you need to change the relative path to icons in it.

Html:
- indexTEMPLATE.html includes empty template without content in the main content area.
- all other files includes content based on each type.
- errors - there are more versions of error files and each error file has more versions of images.
- logins - there are two version of logins.

Skins:
Theme has 6 possible skins:
- Default
- White/Green - add class "theme-white" to body element of every page
- Blue Gradient - add class "theme-blue-gradient" to body element of every page
- Amethyst - add class "theme-amethyst" to body element of every page
- Blue - add class "theme-blue" to body element of every page
- Red - add class "theme-red" to body element of every page

Layout Options:
- Fixed Header - add class "fixed-header" to body element of every page
- Fixed Footer - add class "fixed-footer" to body element of every page
- Fixed Left Menu - add class "fixed-leftmenu" to body element of every page