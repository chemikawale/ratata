/*
SQLyog Enterprise v12.4.1 (64 bit)
MySQL - 5.5.27 : Database - ratata
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`ratata` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `ratata`;

/*Table structure for table `hmo_company` */

DROP TABLE IF EXISTS `hmo_company`;

CREATE TABLE `hmo_company` (
  `id` varchar(11) NOT NULL,
  `hospitalid` varchar(10) NOT NULL,
  `name` varchar(20) NOT NULL,
  `code` varchar(20) NOT NULL,
  `hmopercentage` int(3) NOT NULL,
  `patientpercentage` int(3) NOT NULL,
  `createdby` varchar(20) NOT NULL,
  `createdon` datetime NOT NULL,
  `updatedby` varchar(20) NOT NULL,
  `updatedon` datetime NOT NULL,
  `type` varchar(10) DEFAULT NULL,
  PRIMARY KEY (`id`,`hospitalid`,`name`,`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hmo_company` */

insert  into `hmo_company`(`id`,`hospitalid`,`name`,`code`,`hmopercentage`,`patientpercentage`,`createdby`,`createdon`,`updatedby`,`updatedon`,`type`) values ('dZxVKub0Gc','careapp','AVON HEALTHCARE','AVONHEALTH',80,20,'oyewale','2017-08-07 18:45:37','oyewale','2017-08-07 18:45:37','hmo'),('nOUI78gzEr','careapp','FIDELITY PLC','FIDELITY',100,0,'oyewale','2017-08-07 18:46:01','oyewale','2017-08-07 18:46:01','company');

/*Table structure for table `hmo_company_category` */

DROP TABLE IF EXISTS `hmo_company_category`;

CREATE TABLE `hmo_company_category` (
  `id` varchar(11) NOT NULL,
  `hmo_company_id` varchar(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `createdon` datetime DEFAULT NULL,
  `createdby` varchar(20) DEFAULT NULL,
  `updatedon` datetime DEFAULT NULL,
  `updatedby` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`,`hmo_company_id`,`name`),
  KEY `HMO ID` (`hmo_company_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `hmo_company_category` */

insert  into `hmo_company_category`(`id`,`hmo_company_id`,`name`,`createdon`,`createdby`,`updatedon`,`updatedby`) values ('fxhu3kMPIY','nOUI78gzEr','PREMIUM','2017-08-07 18:51:58','oyewale','2017-08-07 18:51:58','oyewale'),('rV7Kr6LQrf','dZxVKub0Gc','BASIC','2017-08-07 20:59:11','oyewale','2017-08-08 12:09:40','oyewale');

/* Procedure structure for procedure `delete_hmo_by_Id` */

/*!50003 DROP PROCEDURE IF EXISTS  `delete_hmo_by_Id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_hmo_by_Id`(
    
    IN id VARCHAR(64)
    )
BEGIN
           delete FROM hmo_company WHERE hmo_company.`id` = id ;
           
	END */$$
DELIMITER ;

/* Procedure structure for procedure `delete_hmo_category_by_Id` */

/*!50003 DROP PROCEDURE IF EXISTS  `delete_hmo_category_by_Id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_hmo_category_by_Id`(
    IN id VARCHAR(30)
    )
BEGIN
  delete FROM `hmo_company_category` WHERE hmo_company_category.`id` = id ;
	
	END */$$
DELIMITER ;

/* Procedure structure for procedure `find_hmo_by_code_with_hospitalId` */

/*!50003 DROP PROCEDURE IF EXISTS  `find_hmo_by_code_with_hospitalId` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `find_hmo_by_code_with_hospitalId`(
    IN code VARCHAR(64),
    IN hospitalId VARCHAR(64)
    )
BEGIN
  select * from hmo_company where hmo_company.`code` = code and hmo_company.`hospitalid` = hospitalId;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `find_hmo_by_hospitalId` */

/*!50003 DROP PROCEDURE IF EXISTS  `find_hmo_by_hospitalId` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `find_hmo_by_hospitalId`(
    
    IN hospitalId VARCHAR(64)
    )
BEGIN
           SELECT * FROM hmo_company WHERE hmo_company.`hospitalid` = hospitalId order by hmo_company.`name` Asc;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `find_hmo_by_Id` */

/*!50003 DROP PROCEDURE IF EXISTS  `find_hmo_by_Id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `find_hmo_by_Id`(
    
    IN id VARCHAR(64)
    )
BEGIN
           SELECT * FROM hmo_company WHERE hmo_company.`id` = id ;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `find_hmo_by_name_with_hospitalId` */

/*!50003 DROP PROCEDURE IF EXISTS  `find_hmo_by_name_with_hospitalId` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `find_hmo_by_name_with_hospitalId`(
    IN name VARCHAR(64),
    IN hospitalId VARCHAR(64)
    )
BEGIN
  select * from hmo_company where hmo_company.`name` = name and hmo_company.`hospitalid` = hospitalId;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `find_hmo_categories_by_hmoId` */

/*!50003 DROP PROCEDURE IF EXISTS  `find_hmo_categories_by_hmoId` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `find_hmo_categories_by_hmoId`(
    IN hmo_company_id VARCHAR(30)
    )
BEGIN
  SELECT * FROM `hmo_company_category` WHERE hmo_company_category.`hmo_company_id` = hmo_company_id ORDER BY hmo_company_category.`name` ASC;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `find_hmo_category_by_hmoId_with_name` */

/*!50003 DROP PROCEDURE IF EXISTS  `find_hmo_category_by_hmoId_with_name` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `find_hmo_category_by_hmoId_with_name`(
    
     IN NAME VARCHAR(64),
    IN hmo_company_id VARCHAR(64)
    )
BEGIN

 select * from `hmo_company_category` where hmo_company_category.`name` = name and hmo_company_category.`hmo_company_id` = hmo_company_id;
 
	END */$$
DELIMITER ;

/* Procedure structure for procedure `find_hmo_category_by_Id` */

/*!50003 DROP PROCEDURE IF EXISTS  `find_hmo_category_by_Id` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `find_hmo_category_by_Id`(
    IN id VARCHAR(30)
    )
BEGIN
  SELECT * FROM `hmo_company_category` WHERE hmo_company_category.`id` = id ;
	END */$$
DELIMITER ;

/* Procedure structure for procedure `save_hmo_category` */

/*!50003 DROP PROCEDURE IF EXISTS  `save_hmo_category` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `save_hmo_category`(
    IN id VARCHAR(11),
    IN NAME VARCHAR(20),
    IN hmo_company_id VARCHAR(20),
    IN createdBy VARCHAR(20),
    IN updatedBy VARCHAR(20),
    IN createdOn DATETIME,
    IN updatedOn DATETIME
    )
BEGIN
	 INSERT INTO hmo_company_category (
	 id,
	     NAME,
	     hmo_company_id,
	     createdby,
	     updatedby,
	     createdon,
	     updatedon
	 
		
	 )
	 VALUES (
	 id,
	    NAME,
	     hmo_company_id,
	     createdby,
	     updatedby,
	     createdon,
	     updatedon
	 
	 );
	

	END */$$
DELIMITER ;

/* Procedure structure for procedure `save_hmo_company` */

/*!50003 DROP PROCEDURE IF EXISTS  `save_hmo_company` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `save_hmo_company`(
    in id varchar(11),
    IN type varchar(10),
    IN name varchar(20),
    IN hospitalId varchar(20),
    IN code varchar(20),
    IN hmoPercentage Integer,
    IN patientPercentage integer,
    IN createdBy varchar(20),
    IN updatedBy Varchar(20),
    In createdOn Datetime,
    IN updatedOn datetime
    )
BEGIN
	 INSert into hmo_company (
	 id,
	     type,
	     name,
	     hospitalid,
	     code,
	     hmopercentage,
	     patientpercentage,
	     createdby,
	     updatedby,
	     createdon,
	     updatedon
	 
		
	 )
	 values (
	 id,
	  TYPE,
	     NAME,
	     hospitalid,
	     CODE,
	     hmopercentage,
	     patientpercentage,
	     createdby,
	     updatedby,
	     createdon,
	     updatedon
	 
	 );
	

	END */$$
DELIMITER ;

/* Procedure structure for procedure `update_hmo_category` */

/*!50003 DROP PROCEDURE IF EXISTS  `update_hmo_category` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `update_hmo_category`(
    in id varchar(11),   
    IN name varchar(20),  
    IN updatedBy Varchar(20),  
    IN updatedOn datetime
)
begin
    
    update ratata.hmo_company_category
   set 
       ratata.hmo_company_category.id= id,  
            ratata.hmo_company_category.name=name,
            ratata.hmo_company_category.updatedBy=updatedBy,
            ratata.hmo_company_category.updatedOn=updatedOn
        

    where ratata.hmo_company_category.id=id ;

end */$$
DELIMITER ;

/* Procedure structure for procedure `update_hmo_company` */

/*!50003 DROP PROCEDURE IF EXISTS  `update_hmo_company` */;

DELIMITER $$

/*!50003 CREATE DEFINER=`root`@`localhost` PROCEDURE `update_hmo_company`(
       in id varchar(11),
    IN type varchar(10),
    IN name varchar(20),
    IN hospitalId varchar(20),
    IN code varchar(20),
    IN hmoPercentage Integer,
    IN patientPercentage integer,   
    IN updatedBy Varchar(20),  
    IN updatedOn datetime
)
begin
    
    update ratata.hmo_company
   set 
       ratata.hmo_company.id= id,
       ratata.hmo_company.type=type,
         
            ratata.hmo_company.name=name,
            ratata.hmo_company.hospitalId=hospitalId,
            ratata.hmo_company.code=code,
            ratata.hmo_company.hmoPercentage=hmoPercentage,
            ratata.hmo_company.patientPercentage=patientPercentage,
            ratata.hmo_company.updatedBy=updatedBy,
            ratata.hmo_company.updatedOn=updatedOn
        

    where ratata.hmo_company.id=id and ratata.hmo_company.hospitalId=hospitalId;

end */$$
DELIMITER ;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
