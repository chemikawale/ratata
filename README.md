RATATA
========

An extracted basic CRUD part of the Health Insurance module of an existing EHR (Electronic Health Records) System.
This part simply caters fo creating, editing and deletion of HMO/Company and also assigning their corresponding categories.

Tools Used
----------

Developed using Java with Spring MVC framework, MySql and Maven.


Running the application
--------
* The application base context is /ratata

Default Database credentials:

* database name  = ratata
* database username = root and the password is empty 

* The ratata.sql in the repository contains the needed database scripts and should execute seamlessly with recent version of MySql/MariDB
* Maven was used to manage the dependencies, therefore should open and run fine with most compartible IDE and corresponding server for deployment(tested with Tomcat8)

 
