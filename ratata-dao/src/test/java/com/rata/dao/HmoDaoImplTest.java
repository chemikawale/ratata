/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rata.dao;

import com.rata.dao.implementation.HmoDaoImpl;
import com.rata.dao.model.HmoCategory;
import com.rata.dao.model.HmoCompany;
import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.jdbc.JdbcTestUtils;

import java.util.Date;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 *
 * @author Oyewale
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test/test-context.xml"})
public class HmoDaoImplTest {

    @Autowired
    private HmoDaoImpl hmoDao;

    public HmoDaoImplTest() {
    }

    public  void deleteTestData() {

        JdbcTestUtils.deleteFromTableWhere(hmoDao.getJdbcTemplate(), "hmo_company", "id = '" + getHmoCompany().getId() + "'");
        JdbcTestUtils.deleteFromTableWhere(hmoDao.getJdbcTemplate(), "hmo_company_category", "id = '" + getHmoCategory().getId() + "'");

    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public  static void tearDownClass() {

    }

    @Before
    public void setUp() {
        deleteTestData();
        System.out.println("calling @ before");
    }

    @After
    public void tearDown()
    {
       deleteTestData();
    }

    public HmoCompany getHmoCompany() {
        HmoCompany hmoCompany = new HmoCompany();
        hmoCompany.setCode("AVON2");
        hmoCompany.setCreatedBy("admin");
        hmoCompany.setCreatedOn(new Date());
        hmoCompany.setHmoPercentage(80);
        hmoCompany.setHospitalId("careapp");
        hmoCompany.setId("11111AA1");
        hmoCompany.setName("AVON2Health");
        hmoCompany.setPatientPercentage(20);
        hmoCompany.setType("HMO");
        hmoCompany.setUpdatedBy("admin");
        hmoCompany.setUpdatedOn(new Date());

        return hmoCompany;
    }

    public HmoCategory getHmoCategory() {
        HmoCategory hmoCategory = new HmoCategory();
        hmoCategory.setCreatedBy("admin");
        hmoCategory.setCreatedOn(new Date());
        hmoCategory.setHmo_company_id("11111AA1");
        hmoCategory.setId("22211");
        hmoCategory.setUpdatedBy("admin");
        hmoCategory.setUpdatedOn(new Date());
        hmoCategory.setName("basics");

        return hmoCategory;
    }

    /**
     * Test of SaveHmoCompany method, of class HmoDaoImpl.
     *
     * @throws Exception
     */
    @Test
    public void testSaveHmoCompany() throws Exception {
        System.out.println("SaveHmoCompany");
        HmoCompany result = hmoDao.SaveHmoCompany(getHmoCompany());
        assertThat(result).isNotNull();
        assertThat(result.getName()).isEqualTo(getHmoCompany().getName());

    }

    /**
     * Test of findHmoCompanyByCodeWithHospitalId method, of class HmoDaoImpl.
     *
     * @throws Exception
     */
    @Test
    public void testFindHmoCompanyByCodeWithHospitalId() throws Exception {
        System.out.println("findHmoCompanyByCodeWithHospitalId");
        hmoDao.SaveHmoCompany(getHmoCompany());
        HmoCompany hmoCompany = hmoDao.findHmoCompanyByCodeWithHospitalId(getHmoCompany().getCode(), getHmoCompany().getHospitalId());
        assertThat(hmoCompany).isNotNull();
        assertThat(hmoCompany.getHospitalId()).isEqualTo("careapp");
        assertThat(hmoCompany.getCode()).isEqualTo("AVON2");

    }

    /**
     * Test of findHmoCompanyByNameWithHospitalId method, of class HmoDaoImpl.
     *
     * @throws Exception
     */
    @Test
    public void testFindHmoCompanyByNameWithHospitalId() throws Exception {
        System.out.println("findHmoCompanyByNameWithHospitalId");
        hmoDao.SaveHmoCompany(getHmoCompany());
        HmoCompany hmoCompany = hmoDao.findHmoCompanyByNameWithHospitalId(getHmoCompany().getName(), getHmoCompany().getHospitalId());
        assertThat(hmoCompany).isNotNull();
        assertThat(hmoCompany.getHospitalId()).isEqualTo("careapp");

    }

    /**
     * Test of findHmoCompanyByHospitalId method, of class HmoDaoImpl.
     *
     * @throws Exception
     */
    @Test
    public void testFindHmoCompanyByHospitalId() throws Exception {
        System.out.println("findHmoCompanyByHospitalId");
        hmoDao.SaveHmoCompany(getHmoCompany());
        List<HmoCompany> result = hmoDao.findHmoCompanyByHospitalId(getHmoCompany().getHospitalId());
        assertThat(result).isNotNull();
        assertThat(result.size() >= 1);

    }

    /**
     * Test of findHmoCompanyById method, of class HmoDaoImpl.
     *
     * @throws Exception
     */
    @Test
    public void testFindHmoCompanyById() throws Exception {

        System.out.println("findHmoCompanyById");
        hmoDao.SaveHmoCompany(getHmoCompany());
        HmoCompany result = hmoDao.findHmoCompanyById(getHmoCompany().getId());
        assertThat(result.getName()).isEqualTo("AVON2Health");
        assertThat(result.getHospitalId()).isEqualTo("careapp");

    }

//    /**
//     * Test of SaveHmoCategory method, of class HmoDaoImpl.
//     *
//     * @throws Exception
//     */
//    @Test
//    public void testSaveHmoCategory() throws Exception {
//        System.out.println("SaveHmoCategory");
//      //  hmoDao.SaveHmoCompany(getHmoCompany());
//        HmoCategory result = hmoDao.SaveHmoCategory(getHmoCategory());
//        assertThat(result).isNotNull();
//        assertThat(result.getName()).isEqualTo(getHmoCategory().getName());
//
//    }
//
//    /**
//     * Test of findHmoCategoryByNameWithHmoId method, of class HmoDaoImpl.
//     *
//     * @throws Exception
//     */
//    @Test
//    public void testFindHmoCategoryByNameWithHmoId() throws Exception {
//        System.out.println("findHmoCategoryByNameWithHmoId");
//
//        hmoDao.SaveHmoCategory(getHmoCategory());
//        HmoCategory result = hmoDao.findHmoCategoryByNameWithHmoId(getHmoCategory().getHmo_company_id(), getHmoCategory().getName());
//        assertThat(result).isNotNull();
//        assertThat(result.getHmo_company_id()).isEqualTo("11111AA1");
//        assertThat(result.getName()).isEqualTo("basics");
//    }
//
//    /**
//     * Test of findHmoCategoryByHmoId method, of class HmoDaoImpl.
//     *
//     * @throws Exception
//     */
//    @Test
//    public void testFindHmoCategoryByHmoId() throws Exception {
//        System.out.println("findHmoCategoryByHmoId");
//
//        hmoDao.SaveHmoCategory(getHmoCategory());
//        List<HmoCategory> result = hmoDao.findHmoCategoryByHmoId(getHmoCompany().getId());
//        assertThat(result).isNotNull();
//        assertThat(result.size() >= 1);
//        assertThat(result).extracting("name").containsExactly("basics");
//    }


}
