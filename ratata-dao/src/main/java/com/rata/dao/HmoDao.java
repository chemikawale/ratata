/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rata.dao;

import com.rata.dao.model.HmoCategory;
import com.rata.dao.model.HmoCompany;

import java.util.List;

/**
 *
 * @author Oyewale
 */
public interface HmoDao {

    public HmoCompany SaveHmoCompany(HmoCompany hmoCompany) throws Exception;

    public HmoCompany findHmoCompanyByCodeWithHospitalId(String code, String hospitalId) throws Exception;

    public HmoCompany findHmoCompanyById(String Id) throws Exception;

    public boolean deleteHmoCompanyById(String Id) throws Exception;

    public HmoCompany findHmoCompanyByNameWithHospitalId(String HmoName, String hospitalId) throws Exception;

    public List<HmoCompany> findHmoCompanyByHospitalId(String hospitalId) throws Exception;

    public HmoCategory SaveHmoCategory(HmoCategory hmoCategory) throws Exception;

    public HmoCategory findHmoCategoryByNameWithHmoId(String hmoId, String categoryName) throws Exception;

    public List<HmoCategory> findHmoCategoryByHmoId(String hmoId) throws Exception;

    public HmoCategory findHmoCategoryById(String Id) throws Exception;

    public boolean deleteHmoCategoryById(String Id) throws Exception;

    public HmoCompany UpdateHmoCompany(HmoCompany hmoCompany) throws Exception;

    public HmoCategory UpdateHmoCategory(HmoCategory hmoCategory) throws Exception;
}
