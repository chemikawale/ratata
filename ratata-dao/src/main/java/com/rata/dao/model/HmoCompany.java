/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rata.dao.model;


import com.rata.dao.utilities.StaticUtilities;

import java.util.Date;

/**
 *
 * @author Oyewale
 */
public class HmoCompany {

    private String hospitalId;
    private String name;
    private String code;
    private String id;
    private int hmoPercentage;
    private int patientPercentage;
    private Date createdOn;
    private Date updatedOn;
    private String type;
    private String updatedBy;
    private String createdBy;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHospitalId() {
        return hospitalId;
    }

    public void setHospitalId(String hospitalId) {
        this.hospitalId = hospitalId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getHmoPercentage() {
        return hmoPercentage;
    }

    public void setHmoPercentage(int hmoPercentage) {
        this.hmoPercentage = hmoPercentage;
    }

    public int getPatientPercentage() {
        return patientPercentage;
    }

    public void setPatientPercentage(int patientPercentage) {
        this.patientPercentage = patientPercentage;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public HmoCompany toUpdateHmoCompany() {

        HmoCompany hmoCompany = new HmoCompany();
        Date presentDate = StaticUtilities.presentDateTime();
        hmoCompany.setUpdatedOn(presentDate);
        hmoCompany.setUpdatedBy(StaticUtilities.defaultUser);
        hmoCompany.setCode(code.trim());
        hmoCompany.setHmoPercentage(hmoPercentage);
        hmoCompany.setPatientPercentage(patientPercentage);
        hmoCompany.setHospitalId(StaticUtilities.hosptialId);
        hmoCompany.setName(name.trim());
        hmoCompany.setType(type.trim());
        hmoCompany.setId(id);

        return hmoCompany;

    }

}
