/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rata.dao.model;


import com.rata.dao.utilities.StaticUtilities;

import java.util.Date;

/**
 *
 * @author Oyewale
 */
public class HmoCategory {

    private String id;
    private String hmo_company_id;
    private String name;
    private String createdBy;
    private String updatedBy;
    private Date createdOn;
    private Date updatedOn;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHmo_company_id() {
        return hmo_company_id;
    }

    public void setHmo_company_id(String hmo_company_id) {
        this.hmo_company_id = hmo_company_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public String getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(String updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }

    public HmoCategory toUpdateHmoCategory() {
        HmoCategory hmoCategory = new HmoCategory();

        Date presentDate = StaticUtilities.presentDateTime();
        hmoCategory.setUpdatedOn(presentDate);
        hmoCategory.setUpdatedBy(StaticUtilities.defaultUser);
        hmoCategory.setId(id);
        hmoCategory.setName(name.trim());

        return hmoCategory;
    }

}
