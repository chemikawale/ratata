/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rata.dao.implementation;


import com.rata.dao.model.HmoCategory;
import com.rata.dao.model.HmoCompany;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.InvalidDataAccessApiUsageException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Repository;
import com.rata.dao.HmoDao;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Oyewale
 */
@Repository
public class HmoDaoImpl implements HmoDao {

    private static final Logger logger = Logger.getLogger("Hmo Company Dao");
    @Autowired
    private DataSource dataSource;

    private JdbcTemplate jdbcTemplate;

    private SimpleJdbcCall simpleJdbcCall;

    @PostConstruct
    public void init() {
        setJdbcTemplate(new JdbcTemplate(dataSource));
        getJdbcTemplate().setResultsMapCaseInsensitive(true);
    }

    @Override
    public HmoCompany SaveHmoCompany(HmoCompany hmoCompany) throws Exception {
        try {
            SqlParameterSource in = new BeanPropertySqlParameterSource(hmoCompany);
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                    .withCatalogName("ratata")
                    .withProcedureName("save_hmo_company");

            simpleJdbcCall.compile();

            Map result = simpleJdbcCall.execute(in);

        } catch (InvalidDataAccessApiUsageException exception) {
            logger.log(Level.WARNING, "Error occur while creating HMO/Company {0}", exception.getMessage());

        }
        return hmoCompany;
    }

    @Override
    public HmoCompany findHmoCompanyByCodeWithHospitalId(String code, String hospitalId) throws Exception {

        try {
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("code", code).addValue("hospitalId", hospitalId);

            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                    .withCatalogName("ratata")
                    .withProcedureName("find_hmo_by_code_with_hospitalId")
                    .returningResultSet("hmocompany", BeanPropertyRowMapper.newInstance(HmoCompany.class));

            simpleJdbcCall.compile();
            Map result = simpleJdbcCall.execute(sqlParameterSource);

            List<HmoCompany> hmoCompany = (List<HmoCompany>) result.get("hmocompany");

            if (hmoCompany.size() > 0) {
                return hmoCompany.get(0);
            } else {

                return null;

            }
        } catch (InvalidDataAccessApiUsageException exception) {
            logger.log(Level.WARNING, "Error occur while finding hmo with hospitalId and code {0}", exception.getMessage());
        }

        return null;
    }

    @Override
    public HmoCompany findHmoCompanyByNameWithHospitalId(String HmoName, String hospitalId) throws Exception {

        try {
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("name", HmoName).addValue("hospitalId", hospitalId);

            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                    .withCatalogName("ratata")
                    .withProcedureName("find_hmo_by_name_with_hospitalId")
                    .returningResultSet("hmocompany", BeanPropertyRowMapper.newInstance(HmoCompany.class));

            simpleJdbcCall.compile();
            Map result = simpleJdbcCall.execute(sqlParameterSource);

            List<HmoCompany> hmoCompany = (List<HmoCompany>) result.get("hmocompany");

            if (hmoCompany.size() > 0) {
                return hmoCompany.get(0);
            } else {

                return null;

            }
        } catch (InvalidDataAccessApiUsageException exception) {
            logger.log(Level.WARNING, "Error occur while finding hmo with hospitalId and name {0}", exception.getMessage());
        }

        return null;
    }

    @Override
    public List<HmoCompany> findHmoCompanyByHospitalId(String hospitalId) throws Exception {
        try {
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("hospitalId", hospitalId);

            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                    .withCatalogName("ratata")
                    .withProcedureName("find_hmo_by_hospitalId")
                    .returningResultSet("hmocompany", BeanPropertyRowMapper.newInstance(HmoCompany.class));

            simpleJdbcCall.compile();
            Map result = simpleJdbcCall.execute(sqlParameterSource);

            List<HmoCompany> hmoCompany = (List<HmoCompany>) result.get("hmocompany");

            if (hmoCompany.size() > 0) {
                return hmoCompany;
            } else {

                return null;

            }
        } catch (InvalidDataAccessApiUsageException exception) {
            logger.log(Level.WARNING, "Error occur while finding hmos by hospitalId {0}", exception.getMessage());
        }

        return null;
    }

    /**
     * @param jdbcTemplate the jdbcTemplate to set
     */
    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * @return the jdbcTemplate
     */
    public JdbcTemplate getJdbcTemplate() {
        return jdbcTemplate;
    }

    @Override
    public HmoCategory findHmoCategoryByNameWithHmoId(String hmoId, String categoryName) throws Exception {

        try {
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("name", categoryName).addValue("hmo_company_id", hmoId);

            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                    .withCatalogName("ratata")
                    .withProcedureName("find_hmo_category_by_hmoId_with_name")
                    .returningResultSet("hmocategory", BeanPropertyRowMapper.newInstance(HmoCategory.class));

            simpleJdbcCall.compile();
            Map result = simpleJdbcCall.execute(sqlParameterSource);

            List<HmoCategory> hmoCategory = (List<HmoCategory>) result.get("hmocategory");

            if (hmoCategory.size() > 0) {
                return hmoCategory.get(0);
            } else {

                return null;

            }
        } catch (InvalidDataAccessApiUsageException exception) {
            logger.log(Level.WARNING, "Error occur while finding hmo with hospitalId and name {0}", exception.getMessage());
        }

        return null;
    }

    @Override
    public List<HmoCategory> findHmoCategoryByHmoId(String hmoId) throws Exception {
        try {
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("hmo_company_id", hmoId);

            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                    .withCatalogName("ratata")
                    .withProcedureName("find_hmo_categories_by_hmoId")
                    .returningResultSet("hmocategories", BeanPropertyRowMapper.newInstance(HmoCategory.class));

            simpleJdbcCall.compile();
            Map result = simpleJdbcCall.execute(sqlParameterSource);

            List<HmoCategory> hmoCategory = (List<HmoCategory>) result.get("hmocategories");

            if (hmoCategory.size() > 0) {
                return hmoCategory;
            } else {

                return null;

            }
        } catch (InvalidDataAccessApiUsageException exception) {
            logger.log(Level.WARNING, "Error occur while finding hmo's category by hmo/company Id {0}", exception.getMessage());
        }

        return null;
    }

    @Override
    public HmoCategory SaveHmoCategory(HmoCategory hmoCategory) throws Exception {
        try {
            SqlParameterSource in = new BeanPropertySqlParameterSource(hmoCategory);
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                    .withCatalogName("ratata")
                    .withProcedureName("save_hmo_category");

            simpleJdbcCall.compile();

            Map result = simpleJdbcCall.execute(in);

        } catch (InvalidDataAccessApiUsageException exception) {
            logger.log(Level.WARNING, "Error occur while creating HMO/Company Category {0}", exception.getMessage());

        }
        return hmoCategory;
    }

    @Override
    public HmoCompany findHmoCompanyById(String Id) throws Exception {

        try {
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("id", Id);

            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                    .withCatalogName("ratata")
                    .withProcedureName("find_hmo_by_Id")
                    .returningResultSet("hmocompany", BeanPropertyRowMapper.newInstance(HmoCompany.class));

            simpleJdbcCall.compile();
            Map result = simpleJdbcCall.execute(sqlParameterSource);

            List<HmoCompany> hmoCompany = (List<HmoCompany>) result.get("hmocompany");

            if (hmoCompany.size() > 0) {
                return hmoCompany.get(0);
            } else {

                return null;

            }
        } catch (InvalidDataAccessApiUsageException exception) {
            logger.log(Level.WARNING, "Error occur while finding hmo by ID {0}", exception.getMessage());
        }

        return null;
    }

    @Override
    public boolean deleteHmoCompanyById(String Id) throws Exception {
        try {
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("id", Id);

            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                    .withCatalogName("ratata")
                    .withProcedureName("delete_hmo_by_Id");

            simpleJdbcCall.compile();
            Map result = simpleJdbcCall.execute(sqlParameterSource);

            return true;

        } catch (InvalidDataAccessApiUsageException exception) {
            logger.log(Level.WARNING, "Error occur while trying to delete hmo by ID {0}", exception.getMessage());
        }

        return true;
    }

    @Override
    public HmoCategory findHmoCategoryById(String Id) throws Exception {
        try {
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("id", Id);

            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                    .withCatalogName("ratata")
                    .withProcedureName("find_hmo_category_by_Id")
                    .returningResultSet("hmocategory", BeanPropertyRowMapper.newInstance(HmoCategory.class));

            simpleJdbcCall.compile();
            Map result = simpleJdbcCall.execute(sqlParameterSource);

            List<HmoCategory> hmoCategory = (List<HmoCategory>) result.get("hmocategory");

            if (hmoCategory.size() > 0) {
                return hmoCategory.get(0);
            } else {

                return null;

            }
        } catch (InvalidDataAccessApiUsageException exception) {
            logger.log(Level.WARNING, "Error occur while finding hmo category by ID {0}", exception.getMessage());
        }

        return null;
    }

    @Override
    public boolean deleteHmoCategoryById(String Id) throws Exception {
        try {
            SqlParameterSource sqlParameterSource = new MapSqlParameterSource().addValue("id", Id);

            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                    .withCatalogName("ratata")
                    .withProcedureName("delete_hmo_category_by_Id");

            simpleJdbcCall.compile();
            Map result = simpleJdbcCall.execute(sqlParameterSource);

            return true;

        } catch (InvalidDataAccessApiUsageException exception) {
            logger.log(Level.WARNING, "Error occur while trying to delete hmo category by ID {0}", exception.getMessage());
        }

        return true;
    }

    @Override
    public HmoCompany UpdateHmoCompany(HmoCompany hmoCompany) throws Exception {
        try {
            SqlParameterSource in = new BeanPropertySqlParameterSource(hmoCompany);
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                    .withCatalogName("ratata")
                    .withProcedureName("update_hmo_company");

            simpleJdbcCall.compile();

            Map result = simpleJdbcCall.execute(in);

        } catch (InvalidDataAccessApiUsageException exception) {
            logger.log(Level.WARNING, "Error occur while updating HMO/Company {0}", exception.getMessage());

        }
        return hmoCompany;
    }

    @Override
    public HmoCategory UpdateHmoCategory(HmoCategory hmoCategory) throws Exception {
        try {
            SqlParameterSource in = new BeanPropertySqlParameterSource(hmoCategory);
            simpleJdbcCall = new SimpleJdbcCall(dataSource)
                    .withCatalogName("ratata")
                    .withProcedureName("update_hmo_category");

            simpleJdbcCall.compile();

            Map result = simpleJdbcCall.execute(in);

        } catch (InvalidDataAccessApiUsageException exception) {
            logger.log(Level.WARNING, "Error occur while updating HMO/Company's category {0}", exception.getMessage());

        }
        return hmoCategory;
    }
}
