<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<html lang="en">
<head>
    <title>RATATA || UPDATE HMO/COMPANY</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <!-- ===== FAVICON =====-->
    <link rel="shortcut icon" href="favicon.ico">
    <!-- ===== CSS =====-->
    <!-- General-->
    <link rel="stylesheet" href="../css/basic.css">
    <link rel="stylesheet" href="../css/general.css">
    <link rel="stylesheet" href="../css/theme.css" class="style-theme">
    <!-- Specific-->
    <link rel="stylesheet" href="../css/addons/theme/select2.css" class="style-theme-addon"/>

  </head>
  <body>
    
  
    <!--SECTION-->
    <section class="l-main-container">
   <!--Left Sidebar Content-->
     <jsp:include page="../includes/menu.jsp" />
      <!--Main Content-->
      <section class="l-container">
        <!--HEADER-->
        <jsp:include page="../includes/navigation.jsp" />
         <div class="l-page-header">
          <h2 class="l-page-title">UPDATE <span> HMO / COMPANY</span></h2>
          
        </div>
        <div class="l-spaced">
          <!--Basic Elements-->
          <div class="l-box l-spaced-bottom">
            <div class="l-box-header">
              <h2 class="l-box-title"><span>HMO / COMPANY</span> Details</h2>
              <ul class="l-box-options">
                <li><a href="#"><i class="fa fa-cogs"></i></a></li>
                <li><a href="#" data-ason-type="fullscreen" data-ason-target=".l-box" data-ason-content="true" class="ason-widget"></a></li>
                <li><a href="#" data-ason-type="refresh" data-ason-target=".l-box" data-ason-duration="1000" class="ason-widget"><i class="fa fa-rotate-right"></i></a></li>
                <li><a href="#" data-ason-type="toggle" data-ason-find=".l-box" data-ason-target=".l-box-body" data-ason-content="true" data-ason-duration="200" class="ason-widget"></a></li>
                  </ul>
            </div>
            <div class="l-box-body l-spaced">
                
                <form:form commandName="viewModel" role="form" action="updatehmo" method="POST" class="form-horizontal">
      <fieldset>
                                                    <c:choose>
                                    <c:when test="${error !=null && !error.isEmpty()}">
                                        <div class="alert alert-warning">                                                
                                            ${error}
                                        </div>
                                    </c:when>
                                </c:choose>
                                                     <c:choose>
                                                    <c:when test="${success !=null && !success.isEmpty()}">
                                                        <div class="alert alert-success">                                                   
                                                            ${success}
                                                        </div>
                                                    </c:when>
                                                </c:choose>
          
           <div class="form-group">
                  <label for="basicSelect" class="col-sm-3 control-label">TYPE: </label>
                  <div class="col-sm-5">
                    <select id="providerSelect"  class="form-control" name="type" >                       
                        <option value="hmo" label="HMO" > HMO </option>
                        <option value="company" label="COMPANY" > COMPANY </option>
                   
                    </select>
                  </div>
                 </div>
           <form:hidden class="form-control col-sm-3" id="id" name="id" path="id"  />
                        
          <div class="form-group">
                   <label class="col-sm-3 control-label" for="name">NAME:</label>
                       <div class="col-sm-5">                
                         <form:input class="form-control col-sm-3" id="name" name="name" path="name" type="text" placeholder="HMO / COMPANY NAME"   required="true"/>
                           </div>
                </div>
               <div class="form-group">
                   <label class="col-sm-3 control-label" for="code">CODE</label>
                       <div class="col-sm-5">                
                         <form:input class="form-control" id="code" name="code" path="code" type="text" placeholder="HMO/COMPANY CODE"   required="true"/>
                          
                       </div>
                </div>
                <div class="form-group">
                   <label class="col-sm-3 control-label" for="hmoPercentage">PERCENTAGE (*HMO %)</label>
                       <div class="col-sm-5">                
                            <form:input class="form-control" id="hmoPercentage" path="hmoPercentage" name="hmoPercentage" type="text" placeholder=" PAYMENT PERCENTAGE BY HMO" pattern="[0-9]{1,3}" required="true" title="Numeric numbers of length 3 or 1"/>
                        
                       </div>
                </div>
                           
                <div class="form-group">
                   <label class="col-sm-3 control-label" for="patientPercentage">PERCENTAGE (*PATIENT %)</label>
                       <div class="col-sm-5">                
                         <form:input class="form-control" id="patientPercentage" name="patientPercentage" path="patientPercentage" type="text" placeholder="PAYMENT PERCENTAGE BY PATIENT"  pattern="[0-9]{1,3}" required="true" title="Numeric numbers of length 3 or 1"/>
                       
                       </div>
                </div>
                    
                   
                  <div class="form-group">
                  <div class="col-sm-offset-4 col-sm-9">
                 
                    <button type="submit" class="btn btn-dark">SAVE</button>
                  </div>
                </div>
               </form:form>
            </div>
          </div>
         
        </div>
        <!--FOOTER-->
           <jsp:include page="../includes/footer.jsp" />
      </section>

    </section>
 
    <!-- jQuery-->
    <script src="../js/basic/jquery.min.js"></script>
    <script src="../js/basic/jquery-migrate.min.js"></script>
    <!-- General-->
    <script src="../js/basic/modernizr.min.js"></script>
    <script src="../js/basic/bootstrap.min.js"></script>
    <script src="../js/shared/jquery.asonWidget.js"></script>
    <script src="../js/plugins/plugins.js"></script>
    <script src="../js/general.js"></script>
  
    <script src="../js/plugins/pageprogressbar/pace.min.js"></script>
    <!-- Specific-->
    <script src="../js/shared/classie.js"></script>
    <script src="../js/shared/perfect-scrollbar.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.autogrow.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.bootstrap-touchspin.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.checkBo.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.checkradios.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.fancySelect.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.maskedinput.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.onoff.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.select2.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.switchery.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.textareaCounter.plugin.js"></script>
    <script src="../js/plugins/tooltip/jquery.tooltipster.min.js"></script>
    <script src="../js/calls/form.elements.js"></script>
    <script src="../js/calls/part.header.1.js"></script>
    <script src="../js/calls/part.sidebar.2.js"></script>
    <script src="../js/calls/part.theme.setting.js"></script>
 
  </body>

</html>