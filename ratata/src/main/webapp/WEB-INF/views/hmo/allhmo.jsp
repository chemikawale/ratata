<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/functions" prefix = "fn" %>
<html lang="en">
  
<head>
    <title>RATATA || ALL HMO / COMPANY</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <!-- ===== FAVICON =====-->
    <link rel="shortcut icon" href="favicon.ico">
    <!-- ===== CSS =====-->
    <!-- General-->
    <link rel="stylesheet" href="../css/basic.css">
    <link rel="stylesheet" href="../css/general.css">
    <link rel="stylesheet" href="../css/theme.css" class="style-theme">
    <!--[if lt IE 9]>
    <script src="js/basic/respond.min.js"></script>
    <script src="js/basic/html5shiv.min.js"></script>
    <![endif]-->
     <style type="text/css">
        .dataTables_filter,.dataTables_info { display: none; }
    </style>
  </head>
  <body>
    
    <!--[if lt IE 9]>
    <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!--SECTION-->
    <section class="l-main-container">
      <!--Left Sidebar Content-->
     <jsp:include page="../includes/menu.jsp" />
      <!--Main Content-->
      <section class="l-container">
        <!--HEADER-->
        <jsp:include page="../includes/navigation.jsp" />
        <div class="l-page-header">
          <h2 class="l-page-title"><span> ALL </span> HMO / COMPANY</h2>
         
        </div>
        <div class="l-spaced">
          <div class="l-row">
            <div class="l-box">
              <div class="l-box-header">
                <h2 class="l-box-title"><span>HMO / COMPANY</span> Details </h2>
                <ul class="l-box-options">
                  <li><a href="#"><i class="fa fa-cogs"></i></a></li>
                  <li><a href="#" data-ason-type="fullscreen" data-ason-target=".l-box" data-ason-content="true" class="ason-widget"></a></li>
                  <li><a href="#" data-ason-type="refresh" data-ason-target=".l-box" data-ason-duration="1000" class="ason-widget"><i class="fa fa-rotate-right"></i></a></li>
                  <li><a href="#" data-ason-type="toggle" data-ason-find=".l-box" data-ason-target=".l-box-body" data-ason-content="true" data-ason-duration="200" class="ason-widget"></a></li>
                  <li><a href="#" data-ason-type="delete" data-ason-target=".l-box" data-ason-content="true" data-ason-animation="fadeOut" class="ason-widget"></a></li>
                </ul>
              </div>
              <div class="l-box-body">
              <table id="dataTableId" cellspacing="0" width="100%" class="display">
                  <thead>
                    <tr>
                      <th>ID</th>
                      <th>TYPE</th>                             
                      <th>NAME</th>
                      <th>CODE</th>
                      <th>HMO (%)</th>  
                      <th>PATIENT (%)</th>  
                      <th>CREATED ON</th>
                      <th>CREATED BY</th>
                       <th>ACTION</th>
                     
                                  
                      
                      
                    </tr>
                  </thead>
                                       <tbody>
                   <c:forEach items="${allHmo}" var="hmo"> 
                       <tr>
                           <td class="tb-col-1">
                                                   <a href="/ratata/hmo/categories?hmoId=<c:out value='${hmo.id}'/>">    <c:out value="${fn:toUpperCase(hmo.id)}" /> </a>
                                                                </td> 
                                                      <td class="tb-col-2">
                                                                    <c:out value="${fn:toUpperCase(hmo.type)}" />
                                                                </td> 
                                                                   <td class="tb-col-3">
                                                                    <c:out value="${hmo.name}" />
                                                                </td> 
                                                                   <td class="tb-col-4">
                                                                    <c:out value="${hmo.code}" />
                                                                </td> 
                                                                <td class="tb-col-5">
                                                                    <c:out value="${hmo.hmoPercentage}" />
                                                                </td>
                                                                   <td class="tb-col-6">
                                                                    <c:out value="${hmo.patientPercentage}" />
                                                                </td> 
                                                                   <td class="tb-col-7">
                                                                    <c:out value="${hmo.createdOn}" />
                                                                </td> 
                                                                   <td class="tb-col-8">
                                                                    <c:out value="${hmo.createdBy}" />
                                                                </td> 
                                                                <td>
                                                                    <a href="/ratata/hmo/deletehmo?hmoId=<c:out value='${hmo.id}'/>"><i class="icon fa fa-times fa-lg" title="DELETE">  </i></a> ||
                                                                      <a href="/ratata/hmo/updatehmo?hmoId=<c:out value='${hmo.id}'/>"><i class="icon fa fa-edit fa-lg" title="EDIT"></i></a>
                                                                </td>                                                      
                                                                                                                               
                                                               
                                                    
                                                                        </tr>
                                                              </c:forEach>
                   
                      </tbody>
                   
                </table>
              </div>
            </div>
          </div>
        </div>
          <jsp:include page="../includes/footer.jsp" />
      </section>
   
    </section>
 <!-- jQuery-->
    <script src="../js/basic/jquery.min.js"></script>
    <script src="../js/basic/jquery-migrate.min.js"></script>
    <!-- General-->
    <script src="../js/basic/modernizr.min.js"></script>
    <script src="../js/basic/bootstrap.min.js"></script>
    <script src="../js/shared/jquery.asonWidget.js"></script>
    <script src="../js/plugins/plugins.js"></script>
    <script src="../js/general.js"></script>

    <script src="../js/plugins/pageprogressbar/pace.min.js"></script>
    <!-- Specific-->
    <script src="../js/shared/classie.js"></script>
    <script src="../js/shared/perfect-scrollbar.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.bootstrap-touchspin.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.checkBo.min.js"></script>
    <script src="../js/plugins/forms/elements/jquery.switchery.min.js"></script>
    <script src="../js/plugins/table/jquery.dataTables.min.js"></script>
    <script src="../js/plugins/tooltip/jquery.tooltipster.min.js"></script>
    <script src="../js/calls/part.header.1.js"></script>
    <script src="../js/calls/part.sidebar.2.js"></script>
    <script src="../js/calls/part.theme.setting.js"></script>
    <script src="../js/calls/table.data.js"></script>
     </body>
</html>