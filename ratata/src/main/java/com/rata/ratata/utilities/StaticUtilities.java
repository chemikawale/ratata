/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rata.ratata.utilities;

import java.util.Date;
import java.util.TimeZone;
import org.apache.commons.lang.RandomStringUtils;

/**
 *
 * @author Oyewale
 */
public class StaticUtilities {

    public static String hosptialId = "careapp";
    public static String defaultUser = "oyewale";

    public static Date presentDateTime() {

        TimeZone.setDefault(TimeZone.getTimeZone("Africa/Lagos"));
        return new Date();
    }

    /**
     * Generating a random ID in numeric format of specified length
     *
     * @param length
     * @return id
     *
     */
    public static String generateId(int length) {
        String id = RandomStringUtils.randomAlphanumeric(length);
        return id;
    }
}
