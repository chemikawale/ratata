/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.rata.ratata.viewmodel;

import com.rata.dao.model.HmoCategory;
import com.rata.ratata.utilities.StaticUtilities;
import java.util.Date;

/**
 *
 * @author Oyewale
 */
public class HmoCategoryViewModel {

    private String hmoCompany;
    private String name;

    public String getHmoCompany() {
        return hmoCompany;
    }

    public void setHmoCompany(String hmoCompany) {
        this.hmoCompany = hmoCompany;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HmoCategory toHmoCategory() {
        HmoCategory hmoCategory = new HmoCategory();

        Date presentDate = StaticUtilities.presentDateTime();
        hmoCategory.setCreatedOn(presentDate);
        hmoCategory.setUpdatedOn(presentDate);
        hmoCategory.setCreatedBy(StaticUtilities.defaultUser);
        hmoCategory.setUpdatedBy(StaticUtilities.defaultUser);
        hmoCategory.setHmo_company_id(hmoCompany.trim());
        hmoCategory.setId(StaticUtilities.generateId(10));
        hmoCategory.setName(name.trim());

        return hmoCategory;
    }
}
