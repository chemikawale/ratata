package com.rata.ratata;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.rata.dao.HmoDao;
import com.rata.dao.model.HmoCategory;
import com.rata.dao.model.HmoCompany;
import com.rata.ratata.utilities.StaticUtilities;
import com.rata.ratata.viewmodel.HmoCategoryViewModel;
import com.rata.ratata.viewmodel.HmoCompanyViewModel;
import java.util.List;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

/**
 *
 * @author Oyewale
 */
@Controller
@RequestMapping(value = "/hmo")
public class HmoController {

    @Autowired
    private HmoDao hmoDao;

    @RequestMapping(value = {"/create"}, method = RequestMethod.GET)
    public ModelAndView createNewHmoCompany(Model model) {
        ModelAndView mav = new ModelAndView("/views/hmo/create");
        mav.addObject("viewModel", new HmoCompanyViewModel());
        mav.addObject("hmocompany", "active");
        return mav;

    }

    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public ModelAndView SaveNewHmoCompany(@ModelAttribute("viewModel") @Valid HmoCompanyViewModel viewModel, HttpSession session, BindingResult result, Model model, RedirectAttributes rdat) throws Exception {
        ModelAndView mav = new ModelAndView("/views/hmo/create");
        mav.addObject("viewModel", new HmoCompanyViewModel());

        if (result.hasErrors()) {

            mav.addObject("error", "Please correct invalid fields");
            return mav;

        }
        HmoCompany hmoCompany = viewModel.toHmoCompany();
        //find first if the name or code existed already with the specified hospital ID
        HmoCompany hmoCompanyByCode = hmoDao.findHmoCompanyByCodeWithHospitalId(hmoCompany.getCode(), hmoCompany.getHospitalId());
        HmoCompany hmoCompanyByName = hmoDao.findHmoCompanyByNameWithHospitalId(hmoCompany.getName(), hmoCompany.getHospitalId());

        if (hmoCompanyByCode != null || hmoCompanyByName != null) {

            mav.addObject("error", "The HMO/Company Name or Code existed already for your Hospital");

        } else {
            hmoDao.SaveHmoCompany(hmoCompany);
            mav.addObject("success", "A New HMO / Company added successfully");

        }
        return mav;
    }

    @RequestMapping(value = {"/createcategory"}, method = RequestMethod.GET)
    public ModelAndView createHmoCategory(Model model) throws Exception {
        ModelAndView mav = new ModelAndView("/views/hmo/createcategory");
        mav.addObject("viewModel", new HmoCompanyViewModel());
        List<HmoCompany> allHmos = hmoDao.findHmoCompanyByHospitalId(StaticUtilities.hosptialId);
        mav.addObject("allHmos", allHmos);

        mav.addObject("hmocompany", "active");
        return mav;

    }

    @RequestMapping(value = "/createcategory", method = RequestMethod.POST)
    public ModelAndView SaveNewHmoCategory(@ModelAttribute("viewModel") @Valid HmoCategoryViewModel viewModel, HttpSession session, BindingResult result, Model model, RedirectAttributes rdat) throws Exception {
        ModelAndView mav = new ModelAndView("/views/hmo/createcategory");
        mav.addObject("viewModel", new HmoCompanyViewModel());
        mav.addObject("hmocompany", "active");
        List<HmoCompany> allHmos = hmoDao.findHmoCompanyByHospitalId(StaticUtilities.hosptialId);
        mav.addObject("allHmos", allHmos);

        if (result.hasErrors()) {

            mav.addObject("error", "Please correct invalid fields");

            return mav;
        }

        HmoCategory hmoCategory = viewModel.toHmoCategory();
        //find first if the name existed for the hmo company already
        HmoCategory hmoCategoryByName = hmoDao.findHmoCategoryByNameWithHmoId(hmoCategory.getHmo_company_id(), hmoCategory.getName());

        if (hmoCategoryByName != null) {

            mav.addObject("error", "The Category Name existed already for the HMO/Company ");

            return mav;
        } else {
            //save the data

            hmoDao.SaveHmoCategory(hmoCategory);

            mav.addObject("success", "A New HMO / Company Category added successfully");
            return mav;
        }

    }

    @RequestMapping(value = {"/allhmo"}, method = RequestMethod.GET)
    public ModelAndView allHmo(Model model) throws Exception {
        ModelAndView mav = new ModelAndView("/views/hmo/allhmo");
        List<HmoCompany> allHmoByHospitalId = hmoDao.findHmoCompanyByHospitalId(StaticUtilities.hosptialId);
        mav.addObject("allHmo", allHmoByHospitalId);
        mav.addObject("hmocompany", "active");
        return mav;

    }

    @RequestMapping(value = {"/categories"}, method = RequestMethod.GET)
    public ModelAndView allCategoryByHmo(@RequestParam("hmoId") String hmoId) throws Exception {
        ModelAndView mav = new ModelAndView("/views/hmo/hmocategories");
        HmoCompany hmoCompany = hmoDao.findHmoCompanyById(hmoId);
        List<HmoCategory> categoriesByHmoId = hmoDao.findHmoCategoryByHmoId(hmoId);
        mav.addObject("hmoCompany", hmoCompany);
        mav.addObject("hmoCategories", categoriesByHmoId);
        mav.addObject("hmocompany", "active");
        return mav;

    }

    @RequestMapping(value = {"/deletehmo"}, method = RequestMethod.GET)
    public String deleteHmoById(@RequestParam("hmoId") String hmoId) throws Exception {

        hmoDao.deleteHmoCompanyById(hmoId);

        return "redirect:/hmo/allhmo";

    }

    @RequestMapping(value = {"/deletecategory"}, method = RequestMethod.GET)
    public String deleteCategoryById(@RequestParam("categoryId") String categoryId, @RequestParam("hmoId") String hmoId) throws Exception {

        hmoDao.deleteHmoCategoryById(categoryId);

        return "redirect:/hmo/categories?hmoId=" + hmoId;
    }

    @RequestMapping(value = {"/updatehmo"}, method = RequestMethod.GET)
    public ModelAndView updateHmoById(@RequestParam("hmoId") String hmoId) throws Exception {

        ModelAndView mav = new ModelAndView("/views/hmo/updatehmo");
        HmoCompany hmoCompany = hmoDao.findHmoCompanyById(hmoId);
        mav.addObject("viewModel", hmoCompany);
        mav.addObject("hmocompany", "active");
        return mav;

    }

    @RequestMapping(value = "/updatehmo", method = RequestMethod.POST)
    public ModelAndView SaveUpdateHmoById(@ModelAttribute("viewModel") @Valid HmoCompany viewModel, HttpSession session, BindingResult result, Model model, RedirectAttributes rdat) throws Exception {
        ModelAndView mav = new ModelAndView("/views/hmo/updatehmo");
        mav.addObject("viewModel", new HmoCompany());

        if (result.hasErrors()) {

            mav.addObject("error", "Please correct invalid fields");
            return mav;

        }
        HmoCompany updateHmoCompany = viewModel.toUpdateHmoCompany();

        hmoDao.UpdateHmoCompany(updateHmoCompany);
        mav.addObject("success", "HMO / Company updated successfully");

        return mav;
    }

    @RequestMapping(value = {"/updatecategory"}, method = RequestMethod.GET)
    public ModelAndView updateCategoryById(@RequestParam("categoryId") String categoryId, @RequestParam("hmoId") String hmoId) throws Exception {

        ModelAndView mav = new ModelAndView("/views/hmo/updatecategory");
        HmoCategory hmoCategory = hmoDao.findHmoCategoryById(categoryId);
        HmoCompany hmoCompany = hmoDao.findHmoCompanyById(hmoId);
        mav.addObject("hmoCompany", hmoCompany);
        mav.addObject("viewModel", hmoCategory);
        mav.addObject("hmocompany", "active");
        return mav;

    }

    @RequestMapping(value = "/updatecategory", method = RequestMethod.POST)
    public ModelAndView SaveUpdateCategoryById(@ModelAttribute("viewModel") @Valid HmoCategory viewModel, HttpSession session, BindingResult result, Model model, RedirectAttributes rdat) throws Exception {
        ModelAndView mav = new ModelAndView("/views/hmo/updatecategory");
        mav.addObject("viewModel", new HmoCategory());

        if (result.hasErrors()) {

            mav.addObject("error", "Please correct invalid fields");
            return mav;

        }
        HmoCategory updateHmoCategory = viewModel.toUpdateHmoCategory();

        hmoDao.UpdateHmoCategory(updateHmoCategory);
        mav.addObject("success", "HMO / Company category updated successfully");

        return mav;
    }
}
